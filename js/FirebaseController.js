var config = {
    apiKey: "AIzaSyBIa3NOXfxoY21Uyx_QYnKBWx7EurQUWsA",
    authDomain: "cde305-assignemnts.firebaseapp.com",
    databaseURL: "https://cde305-assignemnts.firebaseio.com",
    projectId: "cde305-assignemnts",
    storageBucket: "",
    messagingSenderId: "917738443863"
  };
  firebase.initializeApp(config)

define({
    config : config,
  register: function (email,password,name,tel){
      console.log("email: " + email)
      if(!email.includes("@") || !email.includes(".") || email.length <= 2){
        alert("not vaild email address!")
        return false
        
      }else if(password.length <= 5){
        alert('password too weak!')
        return false
      }else if(name.length <= 1){
        alert("please enter your name!")
        return false
      }else if(tel.length < 8 || tel.length > 8){
        alert("please enter the vaild hongkong telephone/mobile number!")
        return false
      }else{
      firebase.auth().createUserWithEmailAndPassword(email, password).then(function(user){
        
      var userInDatabase = firebase.database().ref("users/").child(user.uid)
      userInDatabase.set({
        email: email,
        name: name,
        tel: tel
      })
      alert('register complete! you can login now!')
      loader.loadLogin()
      }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
    alert(errorMessage)
  });   
}
      
  }
  ,
  login: function(email,password){
    
      firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage)
      // ...
     
    });
    var user = firebase.auth().currentUser
    if(user){
      loader.loadforumMain()
      loader.loadLeft()
      
    }
    
  },
  logout: function(){
    firebase.auth().signOut()
      .then(function() {
        // Sign-out successful.
      
      loader.loadforumMain()
      loader.loadLeft()
      alert('sign Out successful!')
        
      })
      .catch(function(error) {
        // An error happened
        alert(error)
      });
    
  }
  ,
  forumNewpost: function forumNewpost(title,msg){
      var database = firebase.database()
      var newpost = database.ref('posts/').push()
      var user = firebase.auth().currentUser
      if(!user){
        alert('you should login first!')
          return false
      }else if(!title){
        alert('title cannot be empty!')
        console.log(title)
        return false
      }else{
        newpost.set({
          title: title,
          msg: msg,
          userId: user.uid,
          email: user.email,
          postTime: new Date().toLocaleString()
        })
        console.log(newpost)
        loader.loadforumMain()
      }
  },
  leftLoginChecker:function(){
    
    var user = firebase.auth().currentUser
    if(user){
       $('#loginButton').hide() 
       $('#logoutButton').show() 
       $('#loginMsg').show()
       var d = firebase.database().ref("users/").child(user.uid)
       d.on('value',function(snapshot){
         var name = snapshot.val().name 
         var email = snapshot.val().email
         $('#loginMsg').html("Hello, <br/>" + name + "<br/>(" + email + ")")
       })
       
    }else{
       $('#loginButton').show() 
       $('#logoutButton').hide() 
       $('#loginMsg').hide()
    }
  }
})
