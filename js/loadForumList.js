/*
    This is the class that for forum when loading.
*/

define({
    //load all forum post
    load: function(){
        var postlist = firebase.database().ref(`/posts/`);
        postlist.orderByValue().on("value", function(snapshot) {
            console.log(snapshot.val());
            var items = [];
            snapshot.forEach(function(data) {
                console.log(data.key);
                items.push(data)
                
            });
            //load all post and sort by newest date and time
            items.reverse()
            items.forEach(function(data){
                var postHtml = ""
                var userInfo = firebase.database().ref("users/").child(data.val().userId)
                userInfo.on('value',function(snapshot){
                postHtml += "<div id='postBlock' style='border-style: dotted;border-width: 1px; padding: 3px;'>"
                postHtml += "<h5>" + snapshot.val().name + " Said in " + data.val().postTime + ":</h5>" 
                postHtml += "<h5>" + data.val().title +  "</h5>" 
                postHtml += "<p>" + data.val().msg + "</p>"
                postHtml += "</div>"
                $('#forumMainList').append(postHtml)
                })
            })
        });
    },
    //checker for check user logged in or not
    checker: function(){
        
    var user = firebase.auth().currentUser
    if(user){
    }else{
        //if not login, redirect to main page
        loader.loadforumMain()
        alert('You should login first!')
    }
    }
})