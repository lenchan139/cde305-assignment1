/*
    This is a loading helper to load diff ajax part.

*/
define({
    //load side bar 
    loadLeft: function loadLeft(){
       $.ajax({
    
         type: "GET",
         url: '/ajax/leftBar.html', // appears as $_GET['id'] @ your backend side
         success: function(data) {
               // data is ur summary
               console.log(data)
              $('#leftBar').html(data);
              firebaseController.leftLoginChecker()
         }
    
       });
    }
    ,
    //load register page
    loadRegister: function loadRegister(){
       $.ajax({
    
         type: "GET",
         url: '/ajax/register.html', // appears as $_GET['id'] @ your backend side
         success: function(data) {
               // data is ur summary
               console.log(data)
              $('#mainContent').html(data);
         }
    
       });
    }
    ,
    //load login page
    loadLogin: function loadLogin(){
        if(firebase.auth().currentUser){
            loader.loadLeft()
            loader.loadforumMain()
        }else{
       $.ajax({
    
         type: "GET",
         url: '/ajax/login.html', // appears as $_GET['id'] @ your backend side
         success: function(data) {
               // data is ur summary
               console.log(data)
              $('#mainContent').html(data);
         }
    
       });
        }
    },
    //load new post page
       loadNewpost: function(){
           if(!firebase.auth().currentUser){
               alert('please login first!')
               return
           }
       $.ajax({
    
         type: "GET",
         url: '/ajax/forumNewpost.html', // appears as $_GET['id'] @ your backend side
         success: function(data) {
               // data is ur summary
               console.log(data)
              $('#mainContent').html(data);
              forumController.checker()
         }
    
       });
    },
    //load main page
       loadforumMain: function(){
       $.ajax({
    
         type: "GET",
         url: '/ajax/forumList.html', // appears as $_GET['id'] @ your backend side
         success: function(data) {
               // data is ur summary
               console.log(data)
              $('#mainContent').html(data);
              forumController.load()
         }
    
       });
    }
   ,
   
       loadPost: function(postId){
       $.ajax({
    
         type: "GET",
         url: '/ajax/forumPost.html', // appears as $_GET['id'] @ your backend side
         success: function(data) {
               // data is ur summary
               console.log(data)
              $('#mainContent').html(data);
         }
    
       });
    }
   
   ,
       loadPost: function(postId){
       $.ajax({
    
         type: "GET",
         url: '/ajax/forumPost.html', // appears as $_GET['id'] @ your backend side
         data: {
             postId: postId
             },
        success: function(data) {
               // data is ur summary
               console.log(data)
              $('#mainContent').html(data);
              
         }
    
       });
    }
   
})